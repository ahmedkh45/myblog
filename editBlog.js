document.addEventListener("DOMContentLoaded", () => {
  function getQueryParamValue(parameter) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    return urlParams.get(parameter);
  }

  const blogId = getQueryParamValue("blogId");
  console.log(blogId);

  fetch(`http://localhost:8000/blogs/${blogId}`)
    .then((response) => response.json())
    .then((blog) => {
      console.log(blog);

      const title = document.getElementById("title");
      title.value = blog.title;

      const author = document.getElementById("author");
      author.value = blog.author;

      const description = document.getElementById("description");
      description.value = blog.description;
    });
  const submitButton = document.getElementById("submit");
  submitButton.addEventListener("click", (event) => {
    event.preventDefault();

    var title = document.getElementById("title").value;
    var author = document.getElementById("author").value;
    var description = document.getElementById("description").value;

    if (title === "" || author === "" || description === "") {
      showNotification("Fill in all fields");
      return;
    }

    var blogId = getQueryParamValue("blogId");

    var data = {
      id: blogId,
      title: title,
      author: author,
      description: description,
    };

    fetch("http://localhost:8000/blogs/" + blogId, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then(function (response) {
        if (response.ok) {
          console.log("Blog updated successfully");

          window.location.replace(`home.html?notification=updated`);
        } else {
          console.error("Error updating blog");
        }
      })
      .catch(function (error) {
        console.error("Error:", error);
      });
  });
  function showNotification(message, type) {
    notificationText.textContent = message;
    notificationCard.classList.add(type === "success" ? "success" : "error");
    notificationCard.classList.add("show");

    setTimeout(() => {
      notificationCard.classList.remove("show");
    }, 2000);
  }
});
