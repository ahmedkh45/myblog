document.addEventListener("DOMContentLoaded", () => {
  const queryParams = new URLSearchParams(window.location.search);
  const blogId = queryParams.get("id");
  const home = document.getElementById("home");
  const addBlog = document.getElementById("addBlog");

  fetch(`http://localhost:8000/blogs/${blogId}`)
    .then((response) => response.json())
    .then((blog) => {
      const blogDetailsContainer = document.getElementById("blogDetails");

      const titleElement = document.createElement("h3");
      titleElement.className = "title";
      titleElement.textContent = blog.title;

      const authorElement = document.createElement("p");
      authorElement.textContent = blog.author;
      authorElement.className = "author";

      const descriptionElement = document.createElement("p");
      descriptionElement.className = "description";
      descriptionElement.textContent = blog.description;

      const div = document.createElement("div");
      div.className = "buttons";

      
      const editButton = document.createElement("button");
      editButton.className = "edit";
      editButton.textContent = "Edit";
      const span = document.createElement("span");
      span.className = "space";
      const deleteButton = document.createElement("button");
      deleteButton.className = "delete";
      deleteButton.textContent = "Delete";

      
      div.appendChild(editButton);
      div.appendChild(span);
      div.appendChild(deleteButton);

      blogDetailsContainer.appendChild(titleElement);
      blogDetailsContainer.appendChild(authorElement);
      blogDetailsContainer.appendChild(descriptionElement);
      blogDetailsContainer.appendChild(div);
      editButton.addEventListener("click", () => {
        
      });

      deleteButton.addEventListener("click", () => {
        fetch(`http://localhost:8000/blogs/${blogId}`, {
          method: "DELETE",
        })
          .then((response) => {
            if (response.ok) {
              console.log("Blog deleted successfully");

              window.location.replace(`home.html?notification=deleted`); 
            } else {
              console.error("Error deleting blog");
            }
          })
          .catch((error) => {
            console.error("Error deleting blog:", error);
          });
      });

      editButton.addEventListener('click', ()=>{
        setTimeout(()=>{
            const url= `editBlog.html?blogId=${blogId}`
            window.location.replace(url);
        },1000)
       
      })
    })
    .catch((error) => {
      console.error("Error fetching blog details:", error);
    });

  home.addEventListener("click", () => {
    console.log("redirecting");
    setTimeout(() => {
      window.location.replace("home.html");
    }, 1000);
  });
  addBlog.addEventListener("click", () => {
    console.log("redirecting");
    setTimeout(() => {
      window.location.replace("AddBlog.html");
    }, 1000);
  });
});
