document.addEventListener("DOMContentLoaded", () => {
  window.addEventListener("load", adjustSliderItems);
  window.addEventListener("resize", adjustSliderItems);
  const sliderContent = document.querySelector(".blog-container-1");
  const sliderItems = document.querySelectorAll(".slider-item");
  const leftArrow = document.querySelector("#left-arrow");
  const rightArrow = document.querySelector("#right-arrow");
  const addBlog = document.getElementById("addBlog");
  const home = document.getElementById("home");
  const urlParams = new URLSearchParams(window.location.search);
  const notificationType = urlParams.get("notification");

  if (notificationType === "updated") {
    showNotification("Blog Updated Successfully");
  } else if (notificationType === "added") {
    showNotification("Blog Added Successfully");
  } else if (notificationType === "deleted") {
    showNotification("Blog Deleted Successfully");
  }

  let currentIndex = 0;
  let blogs = [];
  populateSlider = () => {
    const blogCount = Math.min(blogs.length, 3);
  
    for (let i = 0; i < blogCount; i++) {
      const blogIndex = (currentIndex + i) % blogs.length;
      const blog = blogs[blogIndex];
      const sliderItem = sliderItems[i];
  
      if (sliderItem) {
        sliderItem.innerHTML = `
          <h2>${blog.title}</h2>
          <p>${blog.author}</p>
          <p>${blog.description}</p>
          <button class="details">Details</button>
        `;
  
        const detailsButton = sliderItem.querySelector(".details");
        detailsButton.addEventListener("click", () => {
          window.location.href = `blogDetails.html?id=${blog.id}`;
        });
      }
    }
  
    // Hide remaining slider items
    for (let i = blogCount; i < 3; i++) {
      const sliderItem = sliderItems[i];
      if (sliderItem) {
        sliderItem.style.display = "none";
      }
    }
  };

  fetch("http://localhost:8000/blogs")
    .then((response) => response.json())
    .then((data) => {
      blogs = data;
      populateSlider();

      const blogListContainer = document.getElementById("blogList");
      console.log("data fetched successfully");
      console.log(data);

      data.forEach((blog) => {
        const blogContainer = document.createElement("div");
        blogContainer.className = "blog-container";

        const titleElement = document.createElement("h2");
        titleElement.textContent = blog.title;

        const authorElement = document.createElement("p");
        authorElement.textContent = blog.author;

        const descriptionElement = document.createElement("p");
        descriptionElement.textContent = blog.description;

        const detailsButton = document.createElement("button");
        detailsButton.className = "details";
        detailsButton.textContent = "Details";

        detailsButton.addEventListener("click", () => {
          setTimeout(() => {
            window.location.href = `blogDetails.html?id=${blog.id}`;
          }, 500);
        });

        blogContainer.appendChild(titleElement);
        blogContainer.appendChild(authorElement);
        blogContainer.appendChild(descriptionElement);
        blogContainer.appendChild(detailsButton);

        blogListContainer.appendChild(blogContainer);
      });
    });

  function showNotification(message, type) {
    notificationText.textContent = message;
    notificationCard.classList.add(type === "success" ? "success" : "error");
    notificationCard.classList.add("show");

    setTimeout(() => {
      notificationCard.classList.remove("show");
    }, 2000);
  }
  function adjustSliderItems() {
    const screenWidth = window.innerWidth;

    if (screenWidth < 876 && screenWidth >= 772 && blogs.length >= 2) {
      sliderItems[2].style.display = "none";
      sliderItems[1].style.display = "block";
    } else if (screenWidth < 772 && blogs.length >= 1) {
      sliderItems[2].style.display = "none";
      sliderItems[1].style.display = "none";
      sliderItems[0].style.display = "block";
    } else {
      sliderItems[2].style.display = "block";
      sliderItems[1].style.display = "block";
      sliderItems[0].style.display = "block";
    }
  }

  leftArrow.addEventListener("click", () => {
    currentIndex = (currentIndex - 1 + blogs.length) % blogs.length;
    populateSlider();
  });

  rightArrow.addEventListener("click", () => {
    currentIndex = (currentIndex + 1) % blogs.length;
    populateSlider();
  });
  addBlog.addEventListener("click", function () {
    console.log("redirecting");
    setTimeout(() => {
      window.location.replace("AddBlog.html");
    }, 1000);
  });
  home.addEventListener("click", function () {
    window.location.replace("home.html");
  });
});
