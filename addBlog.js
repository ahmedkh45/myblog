document.addEventListener("DOMContentLoaded", function () {
  const home = document.getElementById("home");
  var submitButton = document.querySelector(".button-17");

  home.addEventListener("click", () => {
    console.log("redirecting");
    setTimeout(() => {
      window.location.replace("home.html");
    }, 1000);
  });

  submitButton.addEventListener("click", (event) => {
    event.preventDefault();

    var title = document.getElementById("title").value;
    var author = document.getElementById("author").value;
    var description = document.getElementById("description").value;
    if (title === "" || author === "" || description === "") {
      showNotification("fill in all fields");
      return;
    }

    var uniqueId = generateUniqueId();

    var data = {
      id: uniqueId,
      title: title,
      author: author,
      description: description,
    };

    fetch("http://localhost:8000/blogs", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    })
      .then(function (response) {
        if (response.ok) {
          console.log("Blog added successfully");

          window.location.replace(`home.html?notification=added`);
        } else {
          console.error("Error adding blog");
        }
      })
      .catch(function (error) {
        console.error("Error:", error);
      });
  });

  generateUniqueId = () => {
    const randomNum = Math.floor(Math.random() * 100);
    return randomNum;
  };

  function showNotification(message, type) {
    notificationText.textContent = message;
    notificationCard.classList.add(type === "success" ? "success" : "error");
    notificationCard.classList.add("show");

    setTimeout(() => {
      notificationCard.classList.remove("show");
    }, 3000);
  }
});
